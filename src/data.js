export default
    {
        name :'',
        headerTagline: [
                        'Hi 👋 You found me! I am Derrick Lau.',
                        'Full-stack Web developer',          
        ],
        contactEmail:'lauf@cardiff.ac.com',
        abouttext: "Hi there I am a MSc student studying Computing. I started developing web apps in 2018, with experience in HTML, CSS, Javascript all the way to modern techs like React and C# asp.net core.",
        aboutImage:'https://live.staticflickr.com/2279/2020187066_f3bca7cbac_b.jpg',
        ShowAboutImage:true,
  
        projects:[
           {
            id: 1,
            title:'Face-detection-app', 
             service:'Web Development (React, Express)', 

             imageSrc:"https://live.staticflickr.com/65535/49356444676_e4b81ad664_h.jpg",

             url:'https://github.com/Derrick-lau/Face-detection-app/'
            },
            {
                id: 2,
                title: 'Library Management System',
                service: 'Web Development (React, Express)',
                imageSrc: "https://live.staticflickr.com/65535/49355991563_3e7d54c7ac_h.jpg",
                url: 'https://github.com/Derrick-lau/LMS/'
            },
            { 
                id: 3,
                title: 'Migrating Library Management System to Java (developing......)',
                service: 'Web Development (React-typescript, Redux, Java Spring boot)',
                imageSrc: "https://live.staticflickr.com/65535/49355991563_3e7d54c7ac_h.jpg",
                url: 'https://github.com/Derrick-lau/LMS-JAVA/'
            },
            { 
                id: 4,
                title: 'Migrating Face-detection-app to ASP.NET CORE (developing......)',
                service: 'Web Development (React-typescript, C# ASP.NET CORE)',
                imageSrc: "https://live.staticflickr.com/65535/49356652602_0cab5ac61c_c.jpg",
                url: 'https://github.com/Derrick-lau/Face-detection-network'
            },
        ],
        social: [
            {   name:'Github',
                url:'https://github.com/Derrick-lau/'},

            {
                name: 'Linkedin',
                url: 'https://www.linkedin.com/in/for-ka-l-551262168/'
            }
        ]
    }
